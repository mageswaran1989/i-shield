# Container Scan Tools

- Vulnerability Static Analysis for Containers : https://github.com/quay/clair
- A service that analyzes docker images and scans for vulnerabilities : https://github.com/anchore/anchore-engine