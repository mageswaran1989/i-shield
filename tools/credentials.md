# Credentials

- [Common patterns](https://github.com/wunderwuzzi23/scratch/blob/master/creds.csv)
`grep -Iirf searchString.txt files/*`
- https://github.com/BurntSushi/ripgrep
- https://github.com/trufflesecurity/trufflehog
- https://about.sourcegraph.com/
- https://github.com/lgandx/Responder
- https://github.com/Kevin-Robertson/Inveigh
- https://github.com/orlyjamie/mimikittenz
- https://github.com/Sysinternals/ProcDump-for-Linux
```
  wget -q https://packages.microsoft.com/config/
ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb
-O packages-microsoft-prod.deb sudo dpkg -i packages-
microsoft-prod.deb
  ```

- https://securityweekly.com/2012/07/18/post-exploitation-recon-with-e/
- https://embracethered.com/blog/posts/passthecookie/

- evilginx2: https://github.com/kgretzky/evilginx2
- Modlishka: https://github.com/drk1wi/Modlishka
- KoiPhish: https://github.com/wunderwuzzi23/KoiPhish
- https://github.com/byt3bl33d3r/CrackMapExec
- https://github.com/wunderwuzzi23/BashSpray