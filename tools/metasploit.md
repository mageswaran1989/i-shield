# Metasploit
https://medium.com/@muchina/what-is-docker-2a21077b98df

## Parrot OS
```
# notworking
sudo docker run \
    --name parrot \
    -it \
    --hostname parrot \
    --network vulnerable \
    --ip="10.0.0.2" \
    --env DISPLAY=$DISPLAY \
    -v /dev/shm:/dev/shm \
    --device /dev/snd \
    --device /dev/dri \
    --mount type=bind,src=/tmp/.X11-unix,dst=/tmp/.X11-unix \
    parrotsec/security:latest \
    /bin/bash
```

```
# working 
docker run \
    --name metasploit \
    --rm \
    -ti \
    --hostname metasploit \
     --network vulnerable \
     -v $PWD/msf:/root/ 
     parrotsec/tools-metasploit
```

```
sudo docker network create vulnerable --attachable --subnet 10.0.0.0/24

# start docker
docker run \
    -it \
    - rm \
    --network vulnerable \
    --ip="10.0.0.3" \
    --name metasploitable \
    --hostname metasploitable2 \
    tleemcjr/metasploitable2 \
    sh -c "/bin/services.sh && bash"
    
docker rm metasploitable2

# get ip address : 10.0.0.3
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' metasploit

sudo ip addr show docker0

use auxiliary/scanner/portscan/tcp
set RHOSTS 10.0.0.3
set PORTS 1-10000
show options
run

use auxiliary/scanner/discovery/udp_sweep
set RHOSTS 10.0.0.3
run

use auxiliary/scanner/ftp/ftp_login
set RHOSTS 10.0.0.3
set USERPASS_FILE 
run 

use auxiliary/scanner/ftp/ftp_version
set RHOSTS 10.0.0.3
run 

use auxiliary/scanner/ftp/anonymous
set RHOSTS 10.0.0.3
run 

use auxiliary/scanner/smb/smb_version
set RHOSTS 10.0.0.3
run 

use auxiliary/scanner/smb/smb_enumusers
set RHOSTS 10.0.0.3
run 
```
