# Identify CDN

`host -t A www.example.com`
`whois <IP-ADDRESS>`

# NMAP

- Verbose option : `-v`
- Basic Scan: `namp <ip.addr>/<url>`
- Ping Scan:  `nmap -sp 192.168.5.0/24`
- Port Scan:  `nmap -p 1-65535 localhost`
- Stealth scan:  `nmap -sS scanme.nmap.org`
- Version scan or Detect service/daemon version:  `nmap -sV localhost`
- Aggressive scan for OS Detection with fast execution
  - `nmap -A -T4 cloudflare.com`
- Port scan: `nmap -p 5432 192.164.0.1`
- Multiple IP Addresses:  `nmap 1.1.1.1 8.8.8.8,9,10,11`
- Scan IP Range: 
  - `nmap 8.8.8.0/28`
  - `nmap 8.8.8.1-14`
  - `nmap 8.8.8.*`
  - `nmap -p 8.8.8.* --exclude 8.8.8.1`
- Scan the most popular ports
  - `nmap --top-ports 20 192.168.1.106`
- Scan from file:  `nmap -iL file.txt`
- Save scan results
  - Text file: `nmap -oN output.txt securitytrails.com`
  - XML file: `nmap -oX output.xml securitytrails.com`
  - All formats: `nmap -oA output.xml securitytrails.com`
- Disable DNS resolution with `-n`
  - `nmap -p 80 -n 8.8.8.8`
- Scan using TCP or UDP protocols
  - TCP: `nmap -sT 192.168.1.1`
  - UDP: `nmap -sU 192.168.1.1`
- CVE aka Vulnerability Scan:  `nmap -Pn --script vuln 192.168.1.105`
- Launching brute force attacks
  - WordPress brute force attack:  `nmap -sV --script http-wordpress-brute --script-args 'userdb=users.txt,passdb=passwds.txt,http-wordpress-brute.hostname=domain.com, http-wordpress-brute.threads=3,brute.firstonly=true' 192.168.1.105`
  - MS-SQL: `nmap -p 1433 --script ms-sql-brute --script-args userdb=customuser.txt,passdb=custompass.txt 192.168.1.105`
  - FTP brute force attack: `nmap --script ftp-brute -p 21 192.168.1.105`
- Malware detection:  `nmap -sV --script=http-malware-host 192.168.1.105`
- Timing
```
Nmap option	    Timing template name	Speed
|--------------|-----------------------|----------------------------
-T4            |                       |
-T2	           | polite	               | 1 packet every 0.4 seconds
-T1	           | sneaky	               | 1 packet every 15 seconds
-T0	           | paranoid	           | 1 packet every 5 minutes
```
- List top 100 ports
`sort -k3 -nr /usr/share/nmap/nmap-services | grep '/tcp' | head -100`

VAPT stands for Vulnerability Assessment & Penetration Testing

```
nmap -T4 -p- <addr>

nmap -n -Pn -sS -p0-65535 -oA output <IP-ADDRESS>

-n (no DNS resolving)
-Pn (no host discovery, treat the host as online)
-sS (perform TCP SYN scan)
-p0-65535 (scan all ports)
-oA (output in all 3 formats – nmap, gnmap, xml)
```

- Scan for web server ports (56 ports) : `14 mins`
`nmap -T1 -n -Pn -sS -p80-90,443,4443,8000-8009,8080-8099,8181,8443,9000,9090-9099 -oA output <IP-ADDRESS>`
- Scan for web server ports and other interesting ports (93 ports): `24 mins`
`nmap -T1 -n -Pn -sS -p21-23,25,53,80-90,111,139,389,443,445,873,1099,1433,1521,1723,2049,2100,2121,3299,3306,3389,3632,4369,4443,5038,5060,5432,5555,5900-5902,5985,6000-6002,6379,6667,8000-8009,8080-8099,8181,8443,9000,9090-9099,9200,27017 -oA output <IP-ADDRESS>`
- Scan for 200 most popular ports: `50 mins`
`nmap -T1 -n -Pn -sS --top-ports 200 -oA output <IP-ADDRESS>`
- Scan for 1000 most popular ports: `4 hours`
`nmap -T1 -n -Pn -sS --top-ports 1000 -oA output <IP-ADDRESS>`


## Netcat

Check a port is open:

```
nc -nvz <IP-ADDRESS> 80

-n (no DNS resolving)
-v (verbose output)
-z (zero-I/O mode, report connection status only)
```

```
while true; do date; nc -nvz <IP-ADDRESS> 80; sleep $((60*60)); done
```


