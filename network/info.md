# OSI

![](osi.png)


A - Application L HTTP, SMPT
P - Presentation : WMV, JPEG, MOV
S - Session : Session management
T - Transport : TCP / UDP
N - Network : IP addresses, routing
D - Data  : Switched, MAC address
P - Physical : Cables, hub, cat6


# CIDR : Classless Inter-Domain Routing


# Subnet

https://www.ipaddressguide.com/cidr
https://www.calculator.net/ip-subnet-calculator.html

Seven Second Subnetting: : https://www.youtube.com/watch?v=ZxAwQB8TZsM

# Linux Commands

```
# know your ip
curl ifconfig.me

arp -a

#
netstat -ano

route

ip a # address
ip n # neighbour
ip r # route

nc -nvlp 7777 # opens a localport and listens on it
```

# Common Ports and Protocols

## TCP
- FTP : 21
- SSH : 22
- Telnet : 23
- SMPT : 25
- DNS : 53
- HTTP : 80 
- HTTPS : 443
- POP3 110
- SMB 139, 445
- IMAP 143

## UDP
- DNS : 53
- DHCP : 67,68
- TFTP : 69
- SNMP : 161

# DNS

What is a DNS MX record?
A DNS 'mail exchange' (MX) record directs email to a mail server. The MX record indicates how email messages should be 
routed in accordance with the Simple Mail Transfer Protocol (SMTP, the standard protocol for all email). 


How to disclose website’s real IP address behind CDN?
Good news is that it is almost always possible. On the other hand, it may require deeper digging and using a 3rd party services which are not always free. Disclosing real IP address behind CDN is a topic beyond the scope of this article, but let’s briefly mention some of the techniques and methods that can be used to do this:

- Examining HTTP headers
- Examining SSL certificates
- Looking into historical DNS records
- Making the website send us an email
- Finding a vulnerability on the website
- Examining target’s known network ranges and IP addresses

Detailed information on those techniques can be find on the following links:

https://www.secjuice.com/finding-real-ips-of-origin-servers-behind-cloudflare-or-tor/
https://nixcp.com/find-real-ip-address-website-powered-by-cloudflare/
https://geekflare.com/find-real-ip-origin-address-of-website/
https://securitytrails.com/blog/ip-address-behind-cloudflare
https://bitofwp.com/security/find-cloudflare-real-ip/
https://www.google.com/search?q=find+the+ip+address+of+a+website+behind+cloudflare

# VPN
Reference: https://www.udemy.com/course/ccnasecuritypreview/learn/lecture/5552600#overview

Features
- We can apply rules and policies to VPN tunneling without applying them to physical chanel 
- Authentication
- Encryption
- Data integrity

- General Routing Encapsulation - GRE

- Authentication Header : AH, IP protocol 51
- Encapsulation Security Payload : ESP, IP protocol 50
  - Data origin authentication
  - Data confidentiality
  - Anti-replay protection
  - Encrypts the data
- Internet Key Exchange (iKE), which negotiated the security parameters and authentication keys 
```
ESP with Tunnel Mode:
| New IP Header | ESP Hdr | Original IP Hdr | Data PAyload | ESP trialer |

AH with Tunnel Mode:
| New IP Header | Auth Head | Original IP Hdr | Data Payload |

ESP and AH combination with Tunnel Mode:
|New IP Hdr | ESP Hdr | Auth Head | Original IP Hdr | Data Payload | ESP Trailer |

```

```
ESP with transport Mode:
| Copy of original IP Header | ESP Hdr | Original IP Hdr | Data PAyload | ESP trialer |

AH with Transport Mode:
| Copy of original IP Header | Auth Head | Original IP Hdr | Data Payload |
```

```bash 
show crypto isakmp policy
```