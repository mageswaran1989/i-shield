# AWS Security Services

# Identify
- [AWS Config](https://aws.amazon.com/config/)
  - Access, Audit and evaluate the configurations of AWS resources
- [System Manager](https://aws.amazon.com/systems-manager/)

## Protection
- [IAM](https://aws.amazon.com/iam/)
- [Cognito](https://aws.amazon.com/cognito/)
- [Secrets Manager](https://aws.amazon.com/secrets-manager/)
- [VPC](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)
- [Firewall-Manager](https://aws.amazon.com/firewall-manager/)
- [Shield](https://aws.amazon.com/shield/)
- [Iot-device-defender](https://aws.amazon.com/iot-device-defender/)
- [KMS](https://aws.amazon.com/kms/)
- [WAF](https://aws.amazon.com/waf/)
  - 
    - Conditions
    - Rule
    - Web ACL
- [Certificate Manager](https://aws.amazon.com/certificate-manager/)

## Investigate
- [CloudWatch](https://aws.amazon.com/cloudwatch/)
- [CloudTrail](https://aws.amazon.com/cloudtrail/)

## Detection
- [Guardduty](https://aws.amazon.com/guardduty/)
  -  Detects activity such as crypto currency mining, credential compromise behavior, or API calls from known malicious IPs.
- [Inspector](https://aws.amazon.com/inspector/)
- [Detective](https://aws.amazon.com/detective/)
  - Amazon Detective makes it easy to analyze, investigate, and quickly identify the root cause of potential security issues or suspicious activities
- [Macie](https://aws.amazon.com/macie/)
  - SSL certificates are a small data files that cryptographically establish an encrypted link between a web server and a browser. This link ensures that all data passed between the web server and browser remain private.
- [Security-hub](https://aws.amazon.com/security-hub/)
  - Aggregate, organize and prioritize security findings

## Complaince
- [CloudHSM](https://aws.amazon.com/cloudhsm/)