# Top Players in CyberSecurity

- [CrowdStrike](https://www.crowdstrike.com/)
- [Proofpoint](https://www.proofpoint.com/us)
- [SecurityTrails](https://securitytrails.com/)
- [LetsEncrypt](https://letsencrypt.org/)


Open Source Security Testing Methodology Manual (OSSTMM)
- https://www.isecom.org/research.html

Open Web Application Security Project (OWASP)
- https://www.owasp.org/index.php/Category:OWASP_Top_Ten_ Project

- https://attack.mitre.org
- https://capec.mitre.org/


Operation:
- https://www.microsoft.com/security/blog/2007/09/11/stride-chart/