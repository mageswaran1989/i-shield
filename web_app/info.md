# Web Request

- Header
- HTTP method
- Query String
- URI
- Body

## Online Tools

- Find subdomains 
  - crt.sh
  - https://github.com/aboul3la/Sublist3r
  - https://github.com/OWASP/Amass
  - PRobe: https://github.com/tomnomnom/httprobe

## Identifying Website Technologies
- https://builtwith.com/
- https://www.wappalyzer.com/
- `whatweb`

## Exploitation

- https://beefproject.com/
- 