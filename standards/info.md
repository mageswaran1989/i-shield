# Standards

- https://www.first.org/cvss/
- https://www.iso.org/isoiec-27001-information-security.html
- https://www.cisecurity.org/
- Common Weakness Enumeration (CWE)
- Common Attack Pattern Enumeration and Classification (CAPEC)
- MITRE ATT&CK
- STRIDE framework
- https://msdn.microsoft.com/en-us/library/windows/desktop/cc307404.aspx


- https://mitre-attack.github.io/attack-navigator/ enterprise/
- https://attack.mitre.org/docs/Comparing_Layers_in_ Navigator.pdf