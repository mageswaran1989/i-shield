# TLS/SSL

## SSL - Secure Sockets Layer
- SSL stands for Secure Sockets Layer, a security protocol that creates an encrypted link between a web server and a web browser.

What is an SSL certificate?
- SSL certificates are a small data files that cryptographically establish an encrypted link between a web server and a browser. This link ensures that all data passed between the web server and browser remain private.

Types of Certificates
- Extended Validation (EV)
- Organisation Validation (OV)
- Domain Validation (DV)
- Wildcard
- Unified Communication (UCC)
- Single Domain